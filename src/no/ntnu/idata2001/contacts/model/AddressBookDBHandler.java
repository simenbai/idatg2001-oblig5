package no.ntnu.idata2001.contacts.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * Represents an Address book containing contacts with contact details. Based on the example in the
 * book "Objects first with Java" by David J. Barnes and Michael Kölling.
 *
 * <p>Each contact is stored in a TreeMap using the phone number as the key.
 *
 * @author David J. Barnes and Michael Kölling and Arne Styve
 * @version 2020.03.16
 */
public class AddressBookDBHandler implements AddressBook, Serializable, Iterable<ContactDetails> {
  // Storage for an arbitrary number of details.
  // We have chosen to use TreeMap instead of HashMap in this example, the
  // main difference being that a TreeMap is sorted. That is, the keys are sorted,
  // so when retrieving an Iterator from a TreeMap-collection, the iterator will
  // iterate in a sorted manner, which wil not be the case for a HashMap.
  // TreeMap is a bit less efficient than a HashMap in terms of searching, du to the
  // sorted order. For more details on the difference:
  // https://javatutorial.net/difference-between-hashmap-and-treemap-in-java

  private final EntityManagerFactory entityManagerFactory;

  /**
   * Creates an instance of the AddressBook, initialising the instance.
   */
  public AddressBookDBHandler() {
    this.entityManagerFactory = Persistence.createEntityManagerFactory("contacts-pu");
  }

  /**
   * Add a new contact to the address book.
   *
   * @param contact The contact to be added.
   */
  public void addContact(ContactDetails contact) {
    if (contact != null) {
      EntityManager entitymanager = entityManagerFactory.createEntityManager();
      entitymanager.getTransaction().begin();

      entitymanager.persist(contact);
      entitymanager.getTransaction().commit();

      entitymanager.close();
    }
  }

  /**
   * Remove the contact with the given phonenumber from the address book. The phone number should be
   * one that is currently in use.
   *
   * @param phoneNumber The phone number to the contact to remove
   */
  public void removeContact(String phoneNumber) {
    EntityManager entitymanager = entityManagerFactory.createEntityManager();
    entitymanager.getTransaction().begin();

    ContactDetails contact = entitymanager.find(ContactDetails.class, phoneNumber);
    entitymanager.remove(contact);
    entitymanager.getTransaction().commit();
    entitymanager.close();
  }

  /**
   * Returns all the contacts as a collection.
   *
   * @return all the contacts as a collection.
   */
  public Collection<ContactDetails> getAllContacts() {
    EntityManager entitymanager = entityManagerFactory.createEntityManager();

    //Scalar function
    Query query = entitymanager.createQuery("SELECT e from ContactDetails e");
    Collection<?> collection = query.getResultList();
    entitymanager.close();

    ArrayList<ContactDetails> newlyCastedArrayList = new ArrayList<>();
    for (Object listObject : collection) {
      newlyCastedArrayList.add((ContactDetails) listObject);
    }
    return newlyCastedArrayList;
  }

  @Override
  public Iterator<ContactDetails> iterator() {
    return getAllContacts().iterator();
  }

  @Override
  public void close() {
    entityManagerFactory.close();
  }
}